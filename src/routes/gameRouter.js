import express from "express"
import * as gameService from "../services/gameService"
import * as saveService from "../services/saveService"
import HttpError from "../middlewares/HttpError"

const router = express.Router()

router.post("/", async function(req, res) {
  const name = req.body.name
  const numberOfPlayers = req.body.numberOfPlayers
  const shuffle = req.body.shuffle || true

  const game = await gameService.initGame(name, numberOfPlayers, shuffle)
  const { _private, ...returnedGame } = game

  res.send(returnedGame)

  saveService.saveGame(returnedGame)
})

router.get("/:id", async function(req, res) {
  try {
    const game = await saveService.loadGame(req.params.id)
    res.json(game)
  } catch (e) {
    throw new HttpError(500, "Can't find the game")
  }
})

export default router
