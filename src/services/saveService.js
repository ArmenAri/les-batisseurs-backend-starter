import writeFile from "../utils/writeFile"
import readFile from "../utils/readFile"
import path from "path"

export function saveGame(game) {
  const gameContent = JSON.stringify(game, null, "\t")
  const PATHNAME = path.join(__dirname, "../storage/", game.id + ".json")

  writeFile(PATHNAME, gameContent)
}

export async function loadGame(gameId) {
  const gameContent = await readFile(
    path.join(__dirname, "../storage/", gameId + ".json")
  )
  return JSON.parse(gameContent)
}
