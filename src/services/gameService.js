import uuidv4 from "uuid"
import * as cardService from "../services/cardService"

export const createGameId = () => uuidv4()

function createPlayersArray(numberOfPlayers, deckWorkers) {
  const players = []
  const apprentices = Array.prototype.slice
    .call(deckWorkers.slice(0, 6))
    .sort(() => Math.random() - 0.5)
  deckWorkers.sort(() => Math.random() - 0.5)
  for (let i = 0; i < numberOfPlayers; ++i) {
    const id = apprentices[i].id
    players.push({
      id: i,
      finishedBuildings: [],
      availableWorkers: [apprentices[i]],
      underConstructionBuildings: [],
      money: 10,
      victoryPoints: 0,
      actions: 3
    })
    deckWorkers.splice(deckWorkers.findIndex(v => v.id === id), 1)
  }
  return players
}

function createWorkersArray(deckWorkers) {
  const workers = []
  const workersArray = Array.prototype.slice
    .call(deckWorkers)
    .sort(() => Math.random() - 0.5)
  for (let i = 0; i < 5; ++i) {
    const id = workersArray[i]
    workers.push(workersArray[i])
    deckWorkers.splice(deckWorkers.findIndex(v => v.id === id), 1)
  }
  return workers
}

function createBuildingsArray(deckBuildings) {
  const buildings = []
  deckBuildings.sort(() => Math.random() - 0.5)
  for (let i = 0; i < 5; ++i) {
    const building = deckBuildings.shift()
    building.workers = []
    buildings.push(building)
  }
  return buildings
}

function getNextBuildingCard(deckBuildings) {
  const building = deckBuildings.shift()
  building.workers = []
  return building
}

function getNextWorkerCard(deckWorkers) {
  return deckWorkers.shift()
}

export async function initGame(name, numberOfPlayers) {
  const deckWorkers = await cardService.importWorkers()
  const deckBuildings = await cardService.importBuildings()
  return {
    _private: {
      deckWorkers,
      deckBuildings
    },
    id: createGameId(),
    currentPlayer: 0,
    moneyAvailable: 0,
    workers: createWorkersArray(deckWorkers),
    buildings: createBuildingsArray(deckBuildings),
    remainingWorkers: deckWorkers.length,
    remainingBuildings: deckBuildings.length,
    nextWorker: getNextWorkerCard(deckWorkers),
    nextBuilding: getNextBuildingCard(deckBuildings),
    done: false,
    name: name,
    createDate: new Date(),
    players: createPlayersArray(numberOfPlayers, deckWorkers)
  }
}
